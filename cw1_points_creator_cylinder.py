import numpy as np
from scipy.stats import norm
from csv import writer
import random


def generate_points(num_points:int=2000):
    theta = random.uniform(0, 2 * np.pi)
    distribution_x = norm(loc=(1300 * np.cos(theta)), scale=0.05)
    distribution_y = norm(loc=(1300 * np.sin(theta)), scale=0.05)
    distribution_z = norm(loc=0, scale=7)

    x = distribution_x.rvs(size=num_points)
    y = distribution_y.rvs(size=num_points)
    z = distribution_z.rvs(size=num_points)

    points = zip(x,y,z)
    return points

if __name__== '__main__':
    cloud_points = generate_points(2000)
    with open('LidarDataCYL.xyz', 'w', encoding='utf-8', newline='\n') as csvfile:
        csvwriter = writer(csvfile)
        #csvwriter.writerow('x', 'y', 'z')
        for p in cloud_points:
            csvwriter.writerow(p)